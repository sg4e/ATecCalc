# ATecCalc

A simple, clean UI for keeping track of the criteria for A-TEC duel rank in *Yu-Gi-Oh! Forbidden Memories*.

By sg4e.

![](screenshot.png)

## How to run

You will need to have installed [Java](https://www.oracle.com/java/technologies/javase-downloads.html) JDK or JRE, version 8 or above. Grab the latest build on the [Releases page](https://gitlab.com/sg4e/ATecCalc/-/releases). Double-click the downloaded `.jar` file to run. Runs on any operating system that supports Java (which is almost all of them).

## Hotkeys

All hotkeys increment their associated value(s) by 1. Hotkeys require that the application is in focus to work; they are not global.

* `F1`: Fusions
* `F4`: Turns
* `F9`: Facedown Plays and Traps Activated
* `F10`: Equips
* `F11`: Pure Magic
* `F12`: Effective Attacks

## How to build from source (for developers)

This project builds with [Gradle](https://gradle.org/). The repo contains shell and batch scripts for bootstrapping Gradle, which will conveniently download all of the build tools for you:

Unix:
```sh
./gradlew build
```

Windows:
```batch
gradlew.bat build
```

The UI was created with [Netbeans](https://netbeans.org/) GUI Builder. The metadata files for the builder are included in the repo.

## Contributing

UI code must be edited with Netbeans GUI Builder so that the builder files and UI changes are always in sync. Feel free to maintain your own fork under the license terms.

## License

Copyright sg4e. Licensed under the [MIT License](https://gitlab.com/sg4e/ATecCalc/-/blob/master/LICENSE).

/*
 * The MIT License
 *
 * Copyright 2018 sg4e.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sg4e.atec;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import static java.awt.event.KeyEvent.*;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author sg4e
 */
public class GUI extends javax.swing.JFrame implements TecListener {

    private final List<TecPanel> tecPanels = new ArrayList<>();
    private final Map<Integer,TecPanel> keymap = new HashMap<>();

    /**
     * Creates new form AutoGUI
     */
    public GUI() {
        initComponents();
        turnTec.setCategoryName("Turns:");
        effectiveAttackTec.setCategoryName("Effective Attacks:");
        defensiveWinsTec.setCategoryName("Defensive Wins:");
        facedownTec.setCategoryName("Facedown Plays:");
        fusionTec.setCategoryName("Fusions:");
        equipTec.setCategoryName("Equips:");
        pureMagicTec.setCategoryName("Pure Magics:");
        trapTec.setCategoryName("Traps Activated:");
        cardsRemainingTec.setCategoryName("Cards Remaining:");
        lifeRemainingTec.setCategoryName("Life Remaining:");

        turnTec.setLogic(new TecLogic(
                new Range<>(0, 4, 12),
                new Range<>(5, 8, 8),
                new Range<>(9, 28, 0),
                new Range<>(29, 32, -8),
                new Range<>(33, 36, -12)
        ));

        effectiveAttackTec.setLogic(new TecLogic(
                new Range<>(0, 1, 4),
                new Range<>(2, 3, 2),
                new Range<>(4, 9, 0),
                new Range<>(10, 19, -2),
                new Range<>(20, 160, -4)
        ));

        defensiveWinsTec.setLogic(new TecLogic(
                /*
                A discovery in late December 2019 by the FM community showed
                that the official Japanese guide had an off-by-one error for the
                defensive-win ranges and their corresponding Duel Rank differentials.
                */
                new Range<>(0, 2-1, 0),
                new Range<>(3-1, 6-1, -10),
                new Range<>(7-1, 10-1, -20),
                new Range<>(11-1, 15-1, -30),
                new Range<>(16-1, 180, -40)
        ));

        facedownTec.setLogic(new TecLogic(
                new Range<>(0, 0, 0),
                new Range<>(1, 10, -2),
                new Range<>(11, 20, -4),
                new Range<>(21, 30, -6),
                new Range<>(31, 36, -8)
        ));

        fusionTec.setLogic(new TecLogic(
                new Range<>(0, 0, 4),
                new Range<>(1, 4, 0),
                new Range<>(5, 9, -4),
                new Range<>(10, 14, -8),
                new Range<>(15, Integer.MAX_VALUE, -12)
        ));

        equipTec.setLogic(new TecLogic(
                new Range<>(0, 0, 4),
                new Range<>(1, 4, 0),
                new Range<>(5, 9, -4),
                new Range<>(10, 14, -8),
                new Range<>(15, Integer.MAX_VALUE, -12)
        ));

        pureMagicTec.setLogic(new TecLogic(
                new Range<>(0, 0, 2),
                new Range<>(1, 3, -4),
                new Range<>(4, 6, -8),
                new Range<>(7, 9, -12),
                new Range<>(10, 36, -16)
        ));

        trapTec.setLogic(new TecLogic(
                new Range<>(0, 0, 2),
                new Range<>(1, 2, -8),
                new Range<>(3, 4, -16),
                new Range<>(5, 6, -24),
                new Range<>(7, 36, -32)
        ));

        cardsRemainingTec.setLogic(new TecLogic(
                new Range<>(40-8, 40, 15),
                new Range<>(40-12, 40-9, 12),
                new Range<>(40-32, 40-13, 0),
                new Range<>(40-36, 40-33, -5),
                new Range<>(40-40, 40-37, -7)
        ) {
            @Override
            public int getResetValue() {
                return 35;
            }
        });
        cardsRemainingTec.setTargetGreater(false);

        lifeRemainingTec.setLogic(new TecLogic(
                new Range<>(8000, 8000, 6),
                new Range<>(7000, 7999, 4),
                new Range<>(1000, 6999, 0),
                new Range<>(100, 999, -5),
                new Range<>(0, 99, -7)
        ) {
            @Override
            public int getResetValue() {
                return 8000;
            }
        });
        lifeRemainingTec.setTargetGreater(false);

        tecPanels.add(turnTec);
        tecPanels.add(effectiveAttackTec);
        tecPanels.add(defensiveWinsTec);
        tecPanels.add(facedownTec);
        tecPanels.add(fusionTec);
        tecPanels.add(equipTec);
        tecPanels.add(pureMagicTec);
        tecPanels.add(trapTec);
        tecPanels.add(cardsRemainingTec);
        tecPanels.add(lifeRemainingTec);
        tecPanels.forEach(tp -> tp.registerTecChangeListener(this));
        setConfig(CONFIG_DEFAULT);
        tecPanels.forEach(TecPanel::reset);

        keymap.put(VK_F4, turnTec);
        keymap.put(VK_F1, fusionTec);
        keymap.put(VK_F9, trapTec);
        keymap.put(VK_F10, equipTec);
        keymap.put(VK_F11, pureMagicTec);
        keymap.put(VK_F12, effectiveAttackTec);

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                if(e.getID() == KEY_PRESSED) {
                    TecPanel activated = keymap.get(e.getKeyCode());
                    if(activated != null)
                        activated.increment();
                    if(activated == trapTec)
                        facedownTec.increment();
                }
                return e.getKeyCode() == VK_F10;
            }
        });
        //java defaults F10 to the menu bar


        pack();
    }

    private void setConfig(int[] values) {
        for(int i = 0; i < values.length; i++) {
            tecPanels.get(i).setTarget(values[i]);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        categoryLabel = new javax.swing.JLabel();
        actualLabel = new javax.swing.JLabel();
        targetLabel = new javax.swing.JLabel();
        turnTec = new sg4e.atec.TecPanel();
        effectiveAttackTec = new sg4e.atec.TecPanel();
        defensiveWinsTec = new sg4e.atec.TecPanel();
        facedownTec = new sg4e.atec.TecPanel();
        fusionTec = new sg4e.atec.TecPanel();
        equipTec = new sg4e.atec.TecPanel();
        pureMagicTec = new sg4e.atec.TecPanel();
        trapTec = new sg4e.atec.TecPanel();
        cardsRemainingTec = new sg4e.atec.TecPanel();
        lifeRemainingTec = new sg4e.atec.TecPanel();
        totalPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        targetRankLabel = new javax.swing.JLabel();
        targetPointsLabel = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        actualPointsLabel = new javax.swing.JLabel();
        actualRankLabel = new javax.swing.JLabel();
        resetButton = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        targetManu = new javax.swing.JMenu();
        choiceDefault = new javax.swing.JMenuItem();
        choiceTraps = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        choiceTrapless1 = new javax.swing.JMenuItem();
        choiceTrapless2 = new javax.swing.JMenuItem();
        choiceNoTraps = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        choiceManual = new javax.swing.JCheckBoxMenuItem();
        exit = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        sourceRepo = new javax.swing.JMenuItem();
        about = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("A-Tec Calculator");

        categoryLabel.setText("Category:");

        actualLabel.setText("Actual:");

        targetLabel.setText("Target:");

        totalPanel.setBackground(java.awt.Color.pink);

        jLabel1.setText("Rank:");

        jLabel2.setText("Points:");

        targetRankLabel.setText("B Tec");

        targetPointsLabel.setText("50");

        jLabel5.setText("Target:");

        jLabel6.setText("Actual:");

        actualPointsLabel.setText("50");

        actualRankLabel.setText("B Tec");

        javax.swing.GroupLayout totalPanelLayout = new javax.swing.GroupLayout(totalPanel);
        totalPanel.setLayout(totalPanelLayout);
        totalPanelLayout.setHorizontalGroup(
            totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(totalPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel1))
            .addGroup(totalPanelLayout.createSequentialGroup()
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(targetPointsLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(actualPointsLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(targetRankLabel)
                    .addComponent(actualRankLabel)))
        );
        totalPanelLayout.setVerticalGroup(
            totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(totalPanelLayout.createSequentialGroup()
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(targetRankLabel)
                    .addComponent(targetPointsLabel)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(totalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(actualPointsLabel)
                    .addComponent(actualRankLabel)))
        );

        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        fileMenu.setText("File");

        targetManu.setText("Target Config");

        choiceDefault.setText("Fusion-centric");
        choiceDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceDefaultActionPerformed(evt);
            }
        });
        targetManu.add(choiceDefault);

        choiceTraps.setText("Trap-centric");
        choiceTraps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceTrapsActionPerformed(evt);
            }
        });
        targetManu.add(choiceTraps);
        targetManu.add(jSeparator1);

        choiceTrapless1.setText("Trapless");
        choiceTrapless1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceTrapless1ActionPerformed(evt);
            }
        });
        targetManu.add(choiceTrapless1);

        choiceTrapless2.setText("Trapless 2");
        choiceTrapless2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceTrapless2ActionPerformed(evt);
            }
        });
        targetManu.add(choiceTrapless2);

        choiceNoTraps.setText("Trapless Meme");
        choiceNoTraps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceNoTrapsActionPerformed(evt);
            }
        });
        targetManu.add(choiceNoTraps);
        targetManu.add(jSeparator2);

        choiceManual.setText("Manual");
        choiceManual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiceManualActionPerformed(evt);
            }
        });
        targetManu.add(choiceManual);

        fileMenu.add(targetManu);

        exit.setText("Exit");
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        fileMenu.add(exit);

        menuBar.add(fileMenu);

        helpMenu.setText("Help");

        sourceRepo.setText("Open Source Repo");
        sourceRepo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sourceRepoActionPerformed(evt);
            }
        });
        helpMenu.add(sourceRepo);

        about.setText("About ATecCalc...");
        about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutActionPerformed(evt);
            }
        });
        helpMenu.add(about);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cardsRemainingTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lifeRemainingTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(categoryLabel)
                                .addGap(86, 86, 86)
                                .addComponent(actualLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(targetLabel))
                            .addComponent(turnTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(effectiveAttackTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(defensiveWinsTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(facedownTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fusionTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(equipTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pureMagicTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(trapTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totalPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(resetButton, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(categoryLabel)
                    .addComponent(actualLabel)
                    .addComponent(targetLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(turnTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(effectiveAttackTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(defensiveWinsTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(facedownTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fusionTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(equipTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pureMagicTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(trapTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cardsRemainingTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lifeRemainingTec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(totalPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        tecPanels.forEach(TecPanel::reset);
    }//GEN-LAST:event_resetButtonActionPerformed

    private void choiceManualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceManualActionPerformed
        tecPanels.forEach(tec -> tec.setTargetEditable(choiceManual.isSelected()));
    }//GEN-LAST:event_choiceManualActionPerformed

    private void choiceDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceDefaultActionPerformed
        setConfig(CONFIG_DEFAULT);
    }//GEN-LAST:event_choiceDefaultActionPerformed

    private void choiceTrapsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceTrapsActionPerformed
        setConfig(CONFIG_TRAPS);
    }//GEN-LAST:event_choiceTrapsActionPerformed

    private void choiceNoTrapsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceNoTrapsActionPerformed
        setConfig(CONFIG_NO_TRAPS_MEME);
    }//GEN-LAST:event_choiceNoTrapsActionPerformed

    private void choiceTrapless1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceTrapless1ActionPerformed
        setConfig(CONFIG_TRAPLESS1);
    }//GEN-LAST:event_choiceTrapless1ActionPerformed

    private void choiceTrapless2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiceTrapless2ActionPerformed
        setConfig(CONFIG_TRAPLESS2);
    }//GEN-LAST:event_choiceTrapless2ActionPerformed

    private void sourceRepoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sourceRepoActionPerformed
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(URI.create(SOURCE_REPO_URL));
            }
            catch (IOException e) {
                displayDialog(JOptionPane.ERROR_MESSAGE, "Could not open browser on system.", "Error Launching Browser");
            }
        }
        else {
            displayDialog(JOptionPane.ERROR_MESSAGE, "Opening links in browser is not supported on this system.", "No Browser Support");
        }
    }//GEN-LAST:event_sourceRepoActionPerformed

    private void aboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutActionPerformed
        String message = String.format("© 2018 sg4e\n\nVersion: %s\n\nLicense: %s", VERSION, LICENSE_NAME);
        displayDialog(JOptionPane.INFORMATION_MESSAGE, message, "About ATecCalc");
    }//GEN-LAST:event_aboutActionPerformed

    private void displayDialog(int type, String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, type);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch(ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem about;
    private javax.swing.JLabel actualLabel;
    private javax.swing.JLabel actualPointsLabel;
    private javax.swing.JLabel actualRankLabel;
    private sg4e.atec.TecPanel cardsRemainingTec;
    private javax.swing.JLabel categoryLabel;
    private javax.swing.JMenuItem choiceDefault;
    private javax.swing.JCheckBoxMenuItem choiceManual;
    private javax.swing.JMenuItem choiceNoTraps;
    private javax.swing.JMenuItem choiceTrapless1;
    private javax.swing.JMenuItem choiceTrapless2;
    private javax.swing.JMenuItem choiceTraps;
    private sg4e.atec.TecPanel defensiveWinsTec;
    private sg4e.atec.TecPanel effectiveAttackTec;
    private sg4e.atec.TecPanel equipTec;
    private javax.swing.JMenuItem exit;
    private sg4e.atec.TecPanel facedownTec;
    private javax.swing.JMenu fileMenu;
    private sg4e.atec.TecPanel fusionTec;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private sg4e.atec.TecPanel lifeRemainingTec;
    private javax.swing.JMenuBar menuBar;
    private sg4e.atec.TecPanel pureMagicTec;
    private javax.swing.JButton resetButton;
    private javax.swing.JMenuItem sourceRepo;
    private javax.swing.JLabel targetLabel;
    private javax.swing.JMenu targetManu;
    private javax.swing.JLabel targetPointsLabel;
    private javax.swing.JLabel targetRankLabel;
    private javax.swing.JPanel totalPanel;
    private sg4e.atec.TecPanel trapTec;
    private sg4e.atec.TecPanel turnTec;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onTecChange() {
        int actualTotal = 52; //add +2 for Total Annihilation victory
        int targetTotal = 52;
        for(TecPanel tec : tecPanels) {
            actualTotal += tec.getActualPoints();
            targetTotal += tec.getTargetPoints();
        }
        targetPointsLabel.setText("" + targetTotal);
        actualPointsLabel.setText("" + actualTotal);
        targetRankLabel.setText(Range.getValueFromRanges(RANKS, targetTotal));
        actualRankLabel.setText(Range.getValueFromRanges(RANKS, actualTotal));
        if(actualTotal <= 19) //A TEC
            totalPanel.setBackground(Color.GREEN);
        else
            totalPanel.setBackground(Color.PINK);
    }

    private static final List<Range<String>> RANKS = new ArrayList<>();
    static {
        RANKS.add(new Range<>(90, 99, "S POW"));
        RANKS.add(new Range<>(80, 89, "A POW"));
        RANKS.add(new Range<>(70, 79, "B POW"));
        RANKS.add(new Range<>(60, 69, "C POW"));
        RANKS.add(new Range<>(50, 59, "D POW"));
        RANKS.add(new Range<>(40, 49, "D TEC"));
        RANKS.add(new Range<>(30, 39, "C TEC"));
        RANKS.add(new Range<>(20, 29, "B TEC"));
        RANKS.add(new Range<>(10, 19, "A TEC"));
        RANKS.add(new Range<>(0, 9, "S TEC"));
    }

    private static final int[] CONFIG_DEFAULT = new int[] {9, 4, 0, 1, 15, 1, 1, 1, 3, 6999};
    private static final int[] CONFIG_TRAPS = new int[] {9, 4, 0, 1, 5, 1, 1, 3, 3, 6999};
    private static final int[] CONFIG_NO_TRAPS_MEME = new int[] {9, 4, 0, 1, 15, 1, 4, 0, 3, 99};
    private static final int[] CONFIG_TRAPLESS1 = new int[] {9, 2, 0, 1, 15, 1, 7, 0, 3, 999};
    private static final int[] CONFIG_TRAPLESS2 = new int[] {9, 4, 2, 1, 15, 1, 1, 0, 3, 6999};

    private static final String VERSION = "1.2.1";
    private static final String SOURCE_REPO_URL = "https://gitlab.com/sg4e/ATecCalc";
    private static final String LICENSE_NAME = "MIT License";
}
